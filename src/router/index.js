import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/client/Home.vue'
import Catalogue from '../components/client/Catalogue.vue'
import Conf from '../components/client/Conf.vue'
import ShopBag from '../components/client/ShopBag.vue'
import WhishList from '../components/client/WhishList.vue'
import Product from '../components/client/Product.vue'
//import dashBoard from '../components/dashBoard.vue'
import Products from '../components/admin/Productos.vue'
import Users from '../components/admin/Usuarios.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/client/catalogue',
    name: 'catalogue',
    component: Catalogue
  },
  {
    path: '/client/conf',
    name: 'conf',
    component: Conf
  },
  {
    path: '/client/shopbag',
    name: 'ShopBag',
    component: ShopBag
  },
  {
    path: '/client/whishlist',
    name: 'WhishList',
    component: WhishList
  },
  {
    path: '/client/catalogue/product',
    name: 'Product',
    component: Product
  },
  {
    path: '/admin/products',
    name: 'Products',
    component: Products
  },
  {
    path: '/admin/users',
    name: 'users',
    component: Users
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
